package principal;

import static org.junit.Assert.assertTrue;

import java.awt.List;
import org.junit.Test;
import java.util.List;
import org.mockito.*;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Unit test for simple App.
 */
@RunWith(MockitoJUnitRunner.class)
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
    	System.out.println("Ejecutando pruebas..............");
    	String[] args = {"aa","bb","cc"};
    	App.main(args);
        
    	List mockList = Mockito.mock(List.class);
    	mockList.add("hola");
    	mockList.add("mundo");
    	int mockSize = mockList.size();
    	System.out.println("La lista es: " + mockSize);
    	
    	Mockito.when(mockList.size()).thenReturn(200);
    	System.out.println("El tamaño de la lista es: " + mockSize);
    	
    	assertTrue( true );
    }
}
